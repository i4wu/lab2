package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return Float.toString(getValue());
	}
	@Override
	public Temperature toCelsius() {
		float convertedVal = getValue();
		convertedVal = (convertedVal - 32) * ( (float) 5/9);
		return new Celsius(convertedVal);
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
}
