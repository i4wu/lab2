package edu.ucsd.cs110s.temperature;
import java.util.*;

public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return Float.toString(getValue());
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		float convertVal = getValue();
		convertVal *= 9;
		convertVal /= 5;
		convertVal += 32;
		
		return new Celsius(convertVal);
	}
}
